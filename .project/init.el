;; Hey Emacs, this is -*- coding: utf-8 -*-

(require 'hydra)

;; (defvar rh-oauth-in-action/ch-3-ex1-run-client-buffer-name
;;   "*oauth-in-action/ch-3-ex1-client*")

;; (defvar rh-oauth-in-action/ch-3-ex1-run-protected-resource-buffer-name
;;   "*oauth-in-action/ch-3-ex1-protected-resource*")

;; (defvar rh-oauth-in-action/ch-3-ex1-run-authorization-server-buffer-name
;;   "*oauth-in-action/ch-3-ex1-authorization-server*")

;; (defun rh-oauth-in-action/ch-3-ex1-restart-client ()
;;   (interactive)
;;   (rh-project-restart-shell-command
;;    "run-client"
;;    rh-oauth-in-action/ch-3-ex1-run-client-buffer-name))

;; (defun rh-oauth-in-action/ch-3-ex1-restart-protected-resource ()
;;   (interactive)
;;   (rh-project-restart-shell-command
;;    "run-protected-resource"
;;    rh-oauth-in-action/ch-3-ex1-run-protected-resource-buffer-name))

;; (defun rh-oauth-in-action/ch-3-ex1-restart-authorization-server ()
;;   (interactive)
;;   (rh-project-restart-shell-command
;;    "run-authorization-server"
;;    rh-oauth-in-action/ch-3-ex1-run-authorization-server-buffer-name))

;; (defun rh-oauth-in-action/ch-3-ex1-restart-all ()
;;   (interactive)
;;   (rh-oauth-in-action/ch-3-ex1-restart-client)
;;   (rh-oauth-in-action/ch-3-ex1-restart-protected-resource)
;;   (rh-oauth-in-action/ch-3-ex1-restart-authorization-server)
;;   (rh-bs-show-bs-in-bottom-0-side-window "shells"))

;; (defun rh-oauth-in-action/ch-3-ex1-kill-client ()
;;   (interactive)
;;   (rh-project-kill-shell-process
;;    rh-oauth-in-action/ch-3-ex1-run-client-buffer-name))

;; (defun rh-oauth-in-action/ch-3-ex1-kill-protected-resource ()
;;   (interactive)
;;   (rh-project-kill-shell-process
;;    rh-oauth-in-action/ch-3-ex1-run-protected-resource-buffer-name))

;; (defun rh-oauth-in-action/ch-3-ex1-kill-authorization-server ()
;;   (interactive)
;;   (rh-project-kill-shell-process
;;    rh-oauth-in-action/ch-3-ex1-run-authorization-server-buffer-name))

;; (defun rh-oauth-in-action/ch-3-ex1-kill-all ()
;;   (interactive)
;;   (rh-oauth-in-action/ch-3-ex1-kill-client)
;;   (rh-oauth-in-action/ch-3-ex1-kill-protected-resource)
;;   (rh-oauth-in-action/ch-3-ex1-kill-authorization-server))

;; (defun rh-oauth-in-action/ch-3-ex1-hydra-define ()
;;   (defhydra rh-oauth-in-action/ch-3-ex1-hydra (:color blue :columns 4)
;;     "oauth-in-action/ch-3-ex1 project commands"
;;     ("a" rh-oauth-in-action/ch-3-ex1-restart-all
;;      "restart-all")
;;     ("c" rh-oauth-in-action/ch-3-ex1-restart-client
;;      "restart-client")
;;     ("r" rh-oauth-in-action/ch-3-ex1-restart-protected-resource
;;      "restart-protected-resource")
;;     ("z" rh-oauth-in-action/ch-3-ex1-restart-authorization-server
;;      "restart-authorization-server")
;;     ("A" rh-oauth-in-action/ch-3-ex1-kill-all
;;      "kill-all")
;;     ("C" rh-oauth-in-action/ch-3-ex1-kill-client
;;      "kill-client")
;;     ("R" rh-oauth-in-action/ch-3-ex1-kill-protected-resource
;;      "kill-protected-resource")
;;     ("Z" rh-oauth-in-action/ch-3-ex1-kill-authorization-server
;;      "kill-authorization-server")))

;; (rh-oauth-in-action/ch-3-ex1-hydra-define)

;; (define-minor-mode rh-oauth-in-action/ch-3-ex1-mode
;;   "rh-oauth-in-action/ch-3-ex1 project-specific minor mode."
;;   :lighter " rh-oauth-in-action/ch-3-ex1"
;;   :keymap (let ((map (make-sparse-keymap)))
;;             (define-key map (kbd "<f9>") #'rh-oauth-in-action/ch-3-ex1-hydra/body)
;;             map))

;; (add-to-list 'rm-blacklist " rh-oauth-in-action/ch-3-ex1")

(defun grpc-web-1/counter-mali-setup ()
  (let* ((project-root (rh-project-get-root))
         file-rpath ext-js)
    (when project-root
      (setq file-rpath (abbreviate-file-name
                        (expand-file-name buffer-file-name project-root)))
      (cond
       ((or (string-match-p "^#!.*node" (save-excursion
                                          (goto-char (point-min))
                                          (thing-at-point 'line t)))
            (setq ext-js (string-match-p "\\.js\\'" file-rpath)))
        ;; tsserver requires non-.ts files to be manually added to the files
        ;; array in tsconfig.json, otherwise the file will be loaded as part
        ;; of an 'inferred project'. This won't be necessary anymore after
        ;; TypeScript allows defining custom file
        ;; extensions. https://github.com/Microsoft/TypeScript/issues/8328
        (unless ext-js (setq tide-require-manual-setup t))
        (setq rh-js2-additional-externs
              (append rh-js2-additional-externs '("require" "exports" "__dirname")))
        (setq-local tide-tsserver-executable
                    (concat project-root "node_modules/.bin/tsserver"))
        (setq-local flycheck-javascript-eslint-executable
                    (concat project-root "node_modules/.bin/eslint"))
        (rh-setup-javascript-tide))))))
